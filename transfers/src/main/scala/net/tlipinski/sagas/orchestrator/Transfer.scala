package net.tlipinski.sagas.orchestrator

case class Transfer(id: String, from: String, to: String, amount: Int)

